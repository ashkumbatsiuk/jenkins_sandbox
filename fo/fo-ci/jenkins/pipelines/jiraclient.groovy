import groovy.json.JsonSlurperClassic

class JiraClient {
    private String project
    private String jiraUser
    private String jiraPass

    def err = null

    def init(String project, String jiraUser, String jiraPass) {
        this.project = project
        this.jiraUser = jiraUser
        this.jiraPass = jiraPass
    }

    def issue(String issueKey) {
        def url = this.project + "/rest/api/latest/issue/" + issueKey

        try {
            def conn = new URL(url).openConnection()
            def remoteAuth = "Basic " + "${jiraUser}:${jiraPass}".getBytes().encodeBase64().toString()
            conn.setRequestProperty("Authorization", remoteAuth)
            return new JsonSlurperClassic().parse(new InputStreamReader(conn.getInputStream()))
        }
        catch (Exception e) {
            err = e
            return null
        }
     }
}

// inspired by https://stackoverflow.com/questions/44811293/how-to-import-a-file-of-classes-in-a-jenkins-pipeline
Object getProperty(String name){
    return this.getClass().getClassLoader().loadClass(name).newInstance();
}

return this

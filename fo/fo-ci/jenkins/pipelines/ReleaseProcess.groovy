
// create instances of classes defined in other files


class ReleaseBuildState {
    String jiraIssueId
    String jiraIssueDescription
    Map<String, Object> needs
    List<String> featureBranches
}

node{
    ReleaseBuildState state = new ReleaseBuildState()

    def jiraClient
    def jiraParser
    stage("SCM") {
        dir("fo") {
            git branch: "master", credentialsId: "4fc6bdd6-71a1-47aa-9a7c-e6c2d38dff73", url: "https://ashkumbatsiuk@bitbucket.org/ashkumbatsiuk/jenkins_sandbox.git"

            // inspired by https://stackoverflow.com/questions/44811293/how-to-import-a-file-of-classes-in-a-jenkins-pipeline
            jiraClient = load(workspace + '/fo/fo-ci/jenkins/pipelines/jiraclient.groovy').getProperty('JiraClient')
            jiraParser = load(workspace + '/fo/fo-ci/jenkins/pipelines/jiraparser.groovy').getProperty('JiraIssueDescriptionParser')

        }
    }


    def workspace = pwd()
    dir (workspace){

    }

    stage("GATHER_NEEDS") {

    }
    stage("CREATE_RELEASE_BRANCH") {

    }
    stage("FEATURE_MERGE") {

    }
    stage("VERSION_UPDATE") {

    }
    stage("RELEASE_NOTES") {

    }
    stage("RUN_FULL_REGRESSION") {

    }
    stage("STABLE_MERGE") {

    }
}
class JiraIssueDescriptionParser {
    private String text
    HashMap<String,String> needs = new HashMap<String, String>()

    def parse(String text) {
        this.text = text

        text.readLines()
                .findAll { it ==~  /^ *NEED-\d+.*/} // all lines with NEED-XXX with any trailing spaces at the start
                .each {
            def key = it.find(/NEED-\d+/)  // first matching NEED-XXX
            def val = (it - ~/^ *NEED-\d+:? */).trim()  // trimed line with removed NEED-XXX at the start
            needs[key] = val
        }

        return needs
    }
}

// inspired by https://stackoverflow.com/questions/44811293/how-to-import-a-file-of-classes-in-a-jenkins-pipeline
Object getProperty(String name){
    return this.getClass().getClassLoader().loadClass(name).newInstance();
}

return this
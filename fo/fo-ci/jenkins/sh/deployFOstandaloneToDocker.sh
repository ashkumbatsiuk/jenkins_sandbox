export CLOUDSTORAGEACCESSTOKEN=`cat ~/.gradle/gradle.properties | grep 'cloudStorageAccessToken=' | cut -d'=' -f 2 | tr -d '\r\n' | od -A n -t x1 | tr -d '\n' |  sed 's/ //g'`

docker logout https://667083570110.dkr.ecr.us-east-1.amazonaws.com
aws ecr get-login --region us-east-1 | bash

export centosImage=667083570110.dkr.ecr.us-east-1.amazonaws.com/kyriba/centos:6.5-py27
docker pull $centosImage

export CONTAINER_ID=`docker create -e FO_VERSION=${FO_VERSION} -e NO_CUSTOMIZATION=1 --rm -it $centosImage bash`
docker start $CONTAINER_ID

docker exec $CONTAINER_ID wget http://hex:${CLOUDSTORAGEACCESSTOKEN}@kyridev-repository.appspot.com/kyridev-maven/release/com/kyriba/fo/fostandalone/${FO_VERSION}/fostandalone-${FO_VERSION}.jar -O /root/fostandalone-${FO_VERSION}.jar

docker exec $CONTAINER_ID yum -y install java-1.8.0-openjdk
docker exec $CONTAINER_ID /bin/sh -c 'cd /root/ && java -jar /root/fostandalone-${FO_VERSION}.jar'

docker exec $CONTAINER_ID wget https://bootstrap.pypa.io/get-pip.py -O /root/get-pip.py
docker exec $CONTAINER_ID sudo /usr/local/bin/python2.7 /root/get-pip.py

docker exec $CONTAINER_ID mkdir -p /root/.fo-standalone/{hucomm,hudata}
docker exec $CONTAINER_ID mkdir /root/.fo-standalone/hudata/log

docker exec $CONTAINER_ID /bin/sh -c 'pip2.7 install `ls /root/bin/packages/ijson-*`'
docker exec $CONTAINER_ID /bin/sh -c 'pip2.7 install `ls /root/bin/packages/pymongo-*`'

for pymod in dependencies mock framework test iban-utils robot market payments bsi financing-request acknowledgement payments-standalone; do docker exec $CONTAINER_ID pip2.7 install /root/bin/packages/fo-${pymod}-${FO_VERSION}.zip; done

docker stop $CONTAINER_ID

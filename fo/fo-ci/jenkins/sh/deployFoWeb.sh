#!/usr/bin/env bash

foversion=$1
export FOWEB_CONFIG='/home/admin/fo-web.conf'

~/python2.7/bin/foserver stop

rm -rf ~admin/python2.7/build/

echo "Upgrade FO modules"
~/python2.7/bin/pip install -v --upgrade --force-reinstall --egg fo-mock==$foversion fo-dependencies==$foversion fo-payments-standalone==$foversion fo-remittance-viewer==$foversion fo-iban-utils==$foversion
echo "Reinstall FO-WEB"
~/python2.7/bin/pip install -v --no-install fo-web==$foversion | grep -v already | grep == | cut -d' ' -f2 | awk -F'==' '{print $1}' | xargs ~/python2.7/bin/pip uninstall -y
~/python2.7/bin/pip install -v --upgrade fo-web==$foversion
echo "Reinstall FO-WEB Completed"

echo 'starting server'
dtach -n `mktemp -u /tmp/dtach.XXXX` ~/python2.7/bin/foserver start
echo 'server started'
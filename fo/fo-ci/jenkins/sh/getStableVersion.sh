#!/usr/bin/env bash
cd fo

major=$(cat version-stable.gradle | grep 'major =' | sed 's/  major = //g')
minor=$(cat version-stable.gradle | grep 'minor =' | sed 's/  minor = //g')
patch=$(cat version-stable.gradle | grep 'patch =' | sed 's/  patch = //g')
build=$(cat version-stable.gradle | grep 'build =' | sed 's/  build = //g')

STABLE_VERSION=$major.$minor.$patch.$build

echo "STABLE_VERSION=$STABLE_VERSION"
echo "STABLE_VERSION=$STABLE_VERSION" >> $WORKSPACE/ver.properties

cd ..

#!/usr/bin/env bash
echo "<<< lscpu output >>>"
lscpu
echo "<<< check python executable >>>"
python2.7 -V
python2.7 -c "import this"
echo "<<< check virtualenv executable >>>"
virtualenv --version
echo "<<< list all installed packages with versions >>>"
pip list
echo "<<< selfcheck completed successfully >>>"
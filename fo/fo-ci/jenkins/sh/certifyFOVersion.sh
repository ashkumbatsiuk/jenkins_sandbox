#!/usr/bin/env bash

KYRIBA_SOLUTION_BRANCH="develop"

CERTIF_MSG="QA Sandbox certifying Formats version ${QA_TAGGED_VERSION}"

echo "### ${CERTIF_MSG}"
cd fo    
git remote add kyriba-solution "https://KyribaCI:${KyribaCIPassword}@bitbucket.org/kyridev/kyriba-solution.git"
git config user.email "ci@kyriba.com"
git config user.name "KyribaCI"

git checkout develop
git fetch kyriba-solution
sed -i "s/foVersionAccepted=.*/foVersionAccepted=${QA_TAGGED_VERSION}/g" ks-versions.properties
git add ./ks-versions.properties
git commit -m "${CERTIF_MSG}"
git pull --rebase kyriba-solution ${KYRIBA_SOLUTION_BRANCH} 
git push kyriba-solution "${KYRIBA_SOLUTION_BRANCH}"
git remote remove kyriba-solution
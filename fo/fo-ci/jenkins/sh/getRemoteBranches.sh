#!/usr/bin/env bash
# gets a list of all remote origin branches and puts them in the file with last authordate timestamp (which is the date when commit was initially created)
# we don't rely on commit date because of possible rebasing which changes this date.
cd $WORKSPACE/fo
git for-each-ref --sort='-committerdate:rfc2822' --format='%(committerdate:rfc2822);%(refname:strip=3)' refs/remotes/origin/ | tee branches.txt
echo "put branches list under $WORKSPACE/fo/branches.txt"
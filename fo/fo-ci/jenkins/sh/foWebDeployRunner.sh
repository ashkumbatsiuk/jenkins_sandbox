#!/usr/bin/env bash
echo "Deploing fo-web-$QA_TAGGED_VERSION"
echo "$(ls -la ~/.ssh/)"
eval `ssh-agent -s` && ssh-add ~/.ssh/id_rsa_ec2-jenkins
ssh -o StrictHostKeyChecking=no admin@10.42.0.254 'bash -s' < fo/fo-ci/jenkins/sh/deployFoWeb.sh $QA_TAGGED_VERSION
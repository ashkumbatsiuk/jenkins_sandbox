#!/usr/bin/env bash

rnFile='release-notes.html'

if [ $STABLE_BRANCH != "stable" ]; then
   rnFile='release-notes_sp3.html'
fi

echo 'Publishing release notes to:'
echo $WORKSPACE/fo/fo-app-assemble/$rnFile

scp -o StrictHostKeyChecking=no -i ~/.ssh/id_rsa_ec2-jenkins $WORKSPACE/fo/fo-app-assemble/release-notes.html admin@docs.kod.kyriba.com:/var/www/fo/$rnFile

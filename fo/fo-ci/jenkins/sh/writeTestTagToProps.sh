#!/usr/bin/env bash
cd fo

TEST_TAG_FROM_MERGED_BRANCH=$(git log -20 --pretty=tformat:'%s' | grep -E '(NEED-.*|TECH).*' -m 1 |  tr '/' '\n' | tail -n 1 | cut -d' ' -f 1 | tr -d \')
cd ..

echo "TEST_TAG_FROM_MERGED_BRANCH=$TEST_TAG_FROM_MERGED_BRANCH"

if [[ ! -z $TAGS_TO_INCLUDE_MANUAL ]]; then
	TEST_TAG=$TEST_TAG_FROM_MERGED_BRANCH,$TAGS_TO_INCLUDE_MANUAL
else
	TEST_TAG=$TEST_TAG_FROM_MERGED_BRANCH
fi
echo "TEST_TAG=$TEST_TAG"
echo "TEST_TAG=$TEST_TAG" >> $WORKSPACE/ver.properties
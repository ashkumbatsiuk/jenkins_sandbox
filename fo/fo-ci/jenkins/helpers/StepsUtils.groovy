package helpers


class StepsUtils {

    static void getGradleWrapper(context, String wrapper) {
        context.with {
            gradle {
                tasks(wrapper)
                rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
                useWrapper = false
                gradleName(Config.json.jenkins.gradleExecutable)
            }
        }
    }

    static void runPyTests(context) {
        context.with {
            gradle({
                tasks('devBuild')
                rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
            })
            shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/runAllPyTest.sh"))
        }
    }

}

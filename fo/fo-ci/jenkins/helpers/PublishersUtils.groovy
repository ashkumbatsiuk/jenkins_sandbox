package helpers

class PublishersUtils {
    static void mainJobsPublishers(context) {
        context.with {
            extendedEmail {
                recipientList('minskfoteam@kyriba.com')
                defaultSubject('$DEFAULT_SUBJECT')
                defaultContent('$DEFAULT_CONTENT')
                contentType('text/html')
                triggers {
                    always {
                        sendTo {
                            developers()
                            upstreamCommitter()
                            requester()
                        }
                    }
                    failure {
                        sendTo {
                            recipientList()
                        }
                    }
                    fixed {
                        sendTo {
                            recipientList()
                        }
                    }
                }
            }
        }

    }
    static void sandboxJobsPublishers(context) {
        context.with {
            extendedEmail {
                recipientList('minskfoteam@kyriba.com')
                defaultSubject('$DEFAULT_SUBJECT')
                defaultContent('$DEFAULT_CONTENT')
                contentType('text/html')
                triggers {
                    always {
                        sendTo {
                            developers()
                            upstreamCommitter()
                            requester()
                        }
                    }
                    failure {
                        sendTo {
                            upstreamCommitter()
                            requester()
                        }
                    }
                    fixed {
                        sendTo {
                            upstreamCommitter()
                            requester()
                        }
                    }
                }
            }
        }

    }
}

package helpers

class Context {
    
    static def context
    
    static def readFile(String filePathInWorkspace) {
        context.readFileFromWorkspace(filePathInWorkspace)
    }
    
    static def getEnvVar(String variable) {
        def configuration = new HashMap()
        def binding = context.getBinding()
        configuration.putAll(binding.getVariables())
        configuration[variable]
    }
}
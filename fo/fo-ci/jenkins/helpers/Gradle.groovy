package helpers

class Gradle {
    
    static Closure commonProperties = {
        rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
    }

    static Closure defaultSwitches = {
        Config.json.gradle.defaultSwitches.each { switches(it) }
    }

    static Closure excludeSwitches = {
        Config.json.gradle.excludeSwitches.each { switches(it) }
    }

    static Closure unittestSwitches = {
        Config.json.gradle.unittestSwitches.each { switches(it) }
    }

    static Closure robotSwitches = {
        Config.json.gradle.robotSwitches.each { switches(it) }
    }

    static Closure testSwitches(String branch) {

        List options = Config.json.gradle.testSwitches.clone()

        if (options.find({it.startsWith("-Dinclude")})) {
            def original = options.find({it.startsWith("-Dinclude")})
            options.remove(original)
            options.add(buildTagIncludeString(original, branch))
        }
        else {
            def tag = buildTagIncludeString("", branch)
            if (tag) {
                options.add(tag)
            }
        }

        return {options.each { switches(it) }}
    }

    private static buildTagIncludeString(String original, String branchName) {
        if (!original.empty) {
            def include = getIncludeTagFromBranchName(branchName)
            original + (include ? ",$include" : "")
        }
        else {
            def include = getIncludeTagFromBranchName(branchName)
            include ? "-Dinclude=$include" : ""
        }
    }

    static def getIncludeTagFromBranchName(String branchName) {
        if (branchName.split("/").length == 4) {
            branchName.split("/")[-1]
        }
        else ""
    }
    
}
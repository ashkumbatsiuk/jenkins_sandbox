package helpers

import groovy.json.JsonOutput

class EmailHelper {

    private static groupingHelper(String branch, Iterable<String> owners){
        for (owner in owners) {
            if(branch.contains(owner)){
                return owner
            }
        }
        return 'unknown'
    }

    static def getBranchesPerOwner(Iterable<String> branches, Iterable<String> owners){
        def branchesPerOwner = branches.groupBy({ branch -> groupingHelper(branch, owners) })
        return branchesPerOwner
    }

    static def branchesPerOwnerToString(Map<String, List<String>> branchesMap){
        return JsonOutput.prettyPrint(JsonOutput.toJson(branchesMap))
    }
}

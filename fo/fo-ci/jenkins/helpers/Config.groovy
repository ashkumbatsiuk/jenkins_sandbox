package helpers
import groovy.json.JsonSlurper

class Config {

    static def json = new JsonSlurper().parseText(Context.readFile("fo/fo-ci/jenkins/config.json"))

}

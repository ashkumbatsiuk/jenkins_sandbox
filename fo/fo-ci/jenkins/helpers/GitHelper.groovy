package helpers

import groovy.time.TimeCategory
import groovy.time.TimeDuration

import java.text.SimpleDateFormat

class GitHelper {
    private static String separator = ';'
    private static String rfc2822DateFormat = 'EEE, dd MMM yyyy HH:mm:ss Z'


    private static def activeBranchesFilter(String line, Integer activeThresholdDays) {
        def lastActionDateString = line.split(separator)[0]
        def lastActionDate = new SimpleDateFormat(rfc2822DateFormat, Locale.ENGLISH).parse(lastActionDateString)
        TimeDuration td = TimeCategory.minus(new Date(), lastActionDate)
        return td.days < activeThresholdDays
    }

    static def getActiveBranches(Iterable<String> gitBranchesWithDate,
                                 Integer activeThresholdDays,
                                 Iterable<String> branchesToExclude) {

        def activeBranches = gitBranchesWithDate.findAll({ line ->
            activeBranchesFilter(line, activeThresholdDays)
        }).collect({ line -> line.split(separator)[1] })

        def result = activeBranches - branchesToExclude

        return result
    }
}

package helpers


class JobSeedUtils {
    static void defaultConfig(context) {
        context.with{
            jdk(Config.json.jenkins.jdk)
            label(Config.json.jenkins.label)
            logRotator {
                numToKeep(30)
                artifactDaysToKeep(7)
            }
            // Required for pyhub since 2.1.121.1 to unblock `custom` deploy because of sitecustomize.py patching
            environmentVariables(NO_CUSTOMIZATION: '1')
        }
    }

    static void defaultEnvironmentVariables(context) {
        context.with{
            environmentVariables {
                keepBuildVariables(true)
                keepSystemVariables(true)
            }
        }
    }

    static void defaultSCMConfig(context, String branchName=Config.json.git.mainBranch){
        context.with{
            scm {
                git {
                    remote {
                        url(Config.json.git.url)
                        credentials(Config.json.jenkins.bitbucketKeyID) // robot
                    }
                    branch(branchName)
                    extensions {
                        relativeTargetDirectory(Config.json.jenkins.jobWorkspace)
                        cloneOptions {
                            shallow(false)
                            timeout(120)
                        }
                    }
                }
            }
        }
    }
}

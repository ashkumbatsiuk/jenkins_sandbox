package helpers

class WrappersHelper {

    static void injPasswords(context) {
        context.with {
            injectPasswords {
                injectGlobalPasswords()
                maskPasswordParameters()
            }
        }
    }

}

import helpers.Context
import helpers.Config
import helpers.GitHelper
import helpers.Gradle
import helpers.JobSeedUtils
import helpers.PublishersUtils

Context.context = this // we want to expose job dsl to helper classes

String branchesFromParams = Context.getEnvVar("Branches")
def isManual = branchesFromParams != null

if(isManual){
    println "branchesFromParams:"
    println branchesFromParams.split()
    branches = branchesFromParams.split()

}else{
    def allBranches = Context.readFile("${Config.json.jenkins.jobWorkspace}/branches.txt").readLines()
    branches = GitHelper.getActiveBranches(allBranches as Iterable<String>,
            Config.json.git.activeBranchesTimeoutDays as Integer,
            Config.json.git.serviceBranches as Iterable<String>)

}

println "Generating jobs for following branches:"
println branches

branches.each { String branch ->
    job("FO_SANDBOX_${branch.replace("/", '.')}"){
        logRotator {
            artifactDaysToKeep(7)
        }
        JobSeedUtils.defaultConfig delegate
        JobSeedUtils.defaultSCMConfig delegate, branch

        wrappers {
            colorizeOutput()
        }
        triggers {
            scmTrigger {
                scmpoll_spec('* * * * *') //check for commits every minute
                ignorePostCommitHooks(true) //doesn't work anyways (network issue)
            }
        }
        steps {
            shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/checkSystemConfiguration.sh"))
            gradle {
                tasks('wrapper')
                rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
                useWrapper = false
                gradleName(Config.json.jenkins.gradleExecutable)
            }
            gradle({ tasks('devBuild') } << Gradle.commonProperties)
            shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/runAllPyTest.sh"))
            gradle({
                tasks('clean')
                tasks('virtualenv')
                tasks('test')
            } << Gradle.defaultSwitches << Gradle.unittestSwitches << Gradle.robotSwitches << Gradle.testSwitches(branch) << Gradle.commonProperties << Gradle.excludeSwitches)

        }
        publishers {
            archiveJunit '**/test-report/**/*.xml'
            PublishersUtils.sandboxJobsPublishers delegate
        }
    }
}

import helpers.Context
import helpers.Config
import helpers.JobSeedUtils
import helpers.PublishersUtils
import helpers.WrappersHelper
import helpers.StepsUtils

Context.context = this // we want to expose job dsl to helper classes


job("FO_JOB_SEED_SANDBOX_AUTOMATIC_2_2"){
    //new line
    JobSeedUtils.defaultConfig delegate
    JobSeedUtils.defaultSCMConfig delegate
    triggers {
        cron("H 8-20/1 * * *") // 1 A.M.
    }
    steps {
        shell(readFileFromWorkspace("${Config.json.jenkins.ciBuildScripts}/sh/getRemoteBranches.sh"))
        dsl {
            external("${Config.json.jenkins.ciBuildScripts}/SandboxJobFactory.groovy")
            removeAction('DELETE')
            additionalClasspath(Config.json.jenkins.ciBuildScripts)
        }
    }
    publishers {
        PublishersUtils.mainJobsPublishers delegate
    }
}


job("FO_JOB_SEED_SANDBOX_MANUAL"){
    parameters {
        textParam("Branches")
        description('List of branches')
    }
    JobSeedUtils.defaultConfig delegate
    JobSeedUtils.defaultSCMConfig delegate
    steps {
        dsl {
            external("${Config.json.jenkins.ciBuildScripts}/SandboxJobFactory.groovy")
            removeAction('DELETE')
            additionalClasspath(Config.json.jenkins.ciBuildScripts)
        }
    }
    publishers {
        PublishersUtils.mainJobsPublishers delegate
    }
}

job("FO_RELEASE_NOTES"){
    parameters {
        stringParam('STABLE_BRANCH')
    }
    JobSeedUtils.defaultConfig delegate
    JobSeedUtils.defaultSCMConfig delegate, '${STABLE_BRANCH}'

    steps {
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/getStableVersion.sh"))
        environmentVariables {
            propertiesFile('$WORKSPACE/ver.properties')
        }
        buildNameUpdater {
            fromFile(false)
            fromMacro(true)
            macroTemplate('$STABLE_VERSION')
            macroFirst(false)
            buildName('${BUILD_NAME}')
        }

        shell(readFileFromWorkspace("${Config.json.jenkins.ciBuildScripts}/sh/publishReleaseNotes.sh"))
    }
    publishers {
        PublishersUtils.mainJobsPublishers delegate
    }
}

job("FO_DEPLOY_STABLE_TO_PROD_TEST_3"){
    parameters {
        stringParam('STABLE_VERSION')
    }
    JobSeedUtils.defaultConfig delegate
    steps {
        buildNameUpdater{
            fromFile(false)
            fromMacro(true)
            macroTemplate('DEPLOY_${STABLE_VERSION}')
            macroFirst(false)
            buildName('${BUILD_NAME}')
        }
        shell(readFileFromWorkspace("${Config.json.jenkins.ciBuildScripts}/sh/deployStableToProdTest3.sh"))
    }
    publishers {
        PublishersUtils.mainJobsPublishers delegate
    }
}


['FO_STANDALONE_LDU_stable', 'FO_STANDALONE_LDU_qa'].each{jobName ->
    job(jobName){
        parameters {
            stringParam('FO_VERSION', '', 'FO standalone version')
        }

        jdk('Java8u111')
        label('centos7||centos7OnDemand')

        wrappers {
            buildName('${FO_VERSION}')
        }

        steps {
            shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/deployFOstandaloneToDocker.sh"))
        }

        publishers {
            PublishersUtils.mainJobsPublishers delegate
        }
    }
}

job("FO_DEPLOY_qa") {
    parameters {
        stringParam('QA_TAGGED_VERSION')
    }
    JobSeedUtils.defaultEnvironmentVariables delegate
    JobSeedUtils.defaultConfig delegate
    JobSeedUtils.defaultSCMConfig delegate, 'refs/tags/${QA_TAGGED_VERSION}'

    wrappers {
        WrappersHelper.injPasswords delegate
        buildName('QA_DEPLOY_${QA_TAGGED_VERSION}')
    }
    steps {
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/checkSystemConfiguration.sh"))
        StepsUtils.getGradleWrapper delegate, 'wrapper'
        gradle({
            rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
            tasks('clean')
            tasks('virtualenv')
            tasks('build')
            tasks('pypiDeploy')
            tasks('uploadArchives')
            switches('-x test -Dgsutil=/opt/app/gsutil/gsutil -PbuildEnvironement=CI -PbuildVersion=${QA_TAGGED_VERSION}')
            switches(Config.json.gradle.defaultSwitches.join(' '))
        })
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/deployQAtoProdTest2.sh"))
    }
    publishers {
        downstreamParameterized {
            trigger('FO_CERTIFY_FORMATS_VERSION, FO_WEB_APPLICATION_DEPLOYMENT') {
                    condition('SUCCESS')
                parameters {
                    predefinedBuildParameters {
                        properties('QA_TAGGED_VERSION=${QA_TAGGED_VERSION}')
                    }

                }
            }
            trigger('FO_STANDALONE_LDU_qa') {
                condition('SUCCESS')
                parameters {
                    predefinedBuildParameters {
                        properties('FO_VERSION=${QA_TAGGED_VERSION}')
                    }
                }
            }
        }
    }
}

job("FO_PUBLISH_TO_STORAGE_stable") {
    parameters {
        stringParam('STABLE_BRANCH')
    }
    JobSeedUtils.defaultEnvironmentVariables delegate
    JobSeedUtils.defaultConfig delegate
    scm {
        git {
            remote {
                url(Config.json.git.url)
                credentials(Config.json.jenkins.bitbucketKeyID) // robot
            }
            branch('$STABLE_BRANCH')
            extensions {
                cleanBeforeCheckout()
                relativeTargetDirectory(Config.json.jenkins.jobWorkspace)
                cloneOptions {
                    shallow(true)
                    timeout(120)
                }
                localBranch('$STABLE_BRANCH')
            }
        }
    }
    wrappers {
        WrappersHelper.injPasswords delegate
    }
    steps {
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/checkSystemConfiguration.sh"))
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/getStableVersion.sh"))
        environmentVariables {
            propertiesFile('$WORKSPACE/ver.properties')
        }
        buildNameUpdater {
            fromFile(false)
            fromMacro(true)
            macroTemplate('$STABLE_VERSION')
            macroFirst(false)
            buildName('${BUILD_NAME}')
        }
        StepsUtils.getGradleWrapper delegate, 'wrapper -DisStable'
        gradle({
            rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
            tasks('clean')
            tasks('virtualenv')
            tasks('build')
            tasks('pypiDeploy')
            tasks('uploadArchives')
            tasks('tagRevision')
            tasks('updateStableVersion')
            tasks('removeRemote')
            switches('-x test -Dgsutil=/opt/app/gsutil/gsutil -PbuildEnvironement=CI -PisStable=True -DstableBranch=${STABLE_BRANCH}')
            switches(Config.json.gradle.defaultSwitches.join(' '))
        })
    }
    publishers {
        downstreamParameterized {
            trigger('FO_RELEASE_NOTES') {
                condition('SUCCESS')
                parameters {
                    predefinedBuildParameters {
                        properties('STABLE_BRANCH=${STABLE_BRANCH}')
                    }
                }
            }
            trigger('FO_DEPLOY_STABLE_TO_PROD_TEST_3') {
                condition('SUCCESS')
                parameters {
                    predefinedBuildParameters {
                        properties('STABLE_VERSION=${STABLE_VERSION}')
                    }
                }
            }
            trigger('FO_STANDALONE_LDU_stable') {
                condition('SUCCESS')
                parameters {
                    predefinedBuildParameters {
                        properties('FO_VERSION=${STABLE_VERSION}')
                    }
                }
            }
        }
    }
}

job("FO_PUBLISH_TO_STORAGE_CUSTOM_RELEASE") {
    parameters {
        stringParam('BRANCH')
        stringParam('VERSION')
    }
    JobSeedUtils.defaultEnvironmentVariables delegate
    JobSeedUtils.defaultConfig delegate
    scm {
        git {
            remote {
                url(Config.json.git.url)
                credentials(Config.json.jenkins.bitbucketKeyID) // robot
            }
            branch('$BRANCH')
            extensions {
                cleanBeforeCheckout()
                relativeTargetDirectory(Config.json.jenkins.jobWorkspace)
                cloneOptions {
                    shallow(true)
                    timeout(120)
                }
                localBranch('$BRANCH')
            }
        }
    }
    wrappers {
        WrappersHelper.injPasswords delegate
    }
    steps {
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/checkSystemConfiguration.sh"))
        buildNameUpdater {
            fromFile(false)
            fromMacro(true)
            macroTemplate('$VERSION')
            macroFirst(false)
            buildName('${VERSION}')
        }
        StepsUtils.getGradleWrapper delegate, 'wrapper -DisStable'
        gradle({
            rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
            tasks('clean')
            tasks('virtualenv')
            tasks('build')
            tasks('pypiDeploy')
            tasks('uploadArchives')
            tasks('tagRevision')
            switches('-x test -Dgsutil=/opt/app/gsutil/gsutil -PbuildEnvironement=CI -PbuildVersion=${VERSION}')
            switches(Config.json.gradle.defaultSwitches.join(' '))
        })
    }
}


job('FO_SANDBOX_qa') {
    parameters {
        stringParam('TAGS_TO_INCLUDE_MANUAL')
    }
    JobSeedUtils.defaultConfig delegate
    JobSeedUtils.defaultSCMConfig delegate, 'qa'
    triggers {
        scm("* * * * *") {
            ignorePostCommitHooks()
        }
    }
    wrappers {
        colorizeOutput()
        WrappersHelper.injPasswords delegate
    }
    steps {
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/checkSystemConfiguration.sh"))
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/writeQAVersionToProps.sh"))
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/writeTestTagToProps.sh"))
        environmentVariables {
            propertiesFile('$WORKSPACE/ver.properties')
        }
        buildNameUpdater {
            fromFile(false)
            fromMacro(true)
            macroTemplate('${QA_TAGGED_VERSION}-${TEST_TAG}')
            macroFirst(false)
            buildName('${BUILD_NAME}')
        }
        StepsUtils.getGradleWrapper delegate, 'wrapper'
        gradle({
            tasks('devBuild')
            rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
        })
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/runAllPyTest.sh"))
        gradle({
            rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
            tasks('clean')
            tasks('virtualenv')
            tasks('test')
            tasks('tagRevision')
            tasks('removeRemote')
            switches('-Dinclude=${TEST_TAG} -PbuildVersion=${QA_TAGGED_VERSION}')
            switches(Config.json.gradle.defaultSwitches.join(' '))
            switches(Config.json.gradle.unittestSwitches.join(' '))
            switches(Config.json.gradle.testSwitches.join(' '))
            switches(Config.json.gradle.robotSwitches.join(' '))
            switches(Config.json.gradle.excludeSwitches.join())
        })
    }
    publishers {
        archiveJunit '**/test-report/**/*.xml'
        PublishersUtils.mainJobsPublishers delegate
        publishers {
            downstreamParameterized {
                trigger('FO_DEPLOY_qa') {
                    condition('SUCCESS')
                    parameters {
                        predefinedBuildParameters {
                            properties('QA_TAGGED_VERSION=${QA_TAGGED_VERSION}')
                        }

                    }
                }
            }

        }
    }
}

job('FO_WEB_APPLICATION_DEPLOYMENT') {
    parameters {
        stringParam('QA_TAGGED_VERSION')
    }
    JobSeedUtils.defaultConfig delegate
    JobSeedUtils.defaultSCMConfig delegate
    wrappers {
        colorizeOutput()
        buildName('FO_WEB_${QA_TAGGED_VERSION}')
    }
    steps {
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/foWebDeployRunner.sh"))
    }
}

job('FO_FULL_REGRESSION_stable') {
    parameters {
        stringParam('STABLE_BRANCH', 'stable')
    }
    JobSeedUtils.defaultConfig delegate
    JobSeedUtils.defaultSCMConfig delegate, '${STABLE_BRANCH}'
    label("fo_full_regression")
    triggers {
            scm('H 21 * * *')
        }
    steps {
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/checkSystemConfiguration.sh"))
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/getStableVersion.sh"))
        environmentVariables {
            propertiesFile('$WORKSPACE/ver.properties')
        }
        buildNameUpdater {
            fromFile(false)
            fromMacro(true)
            macroTemplate('$STABLE_VERSION')
            macroFirst(false)
            buildName('${BUILD_NAME}')
        }
        StepsUtils.getGradleWrapper delegate, 'wrapper'
        StepsUtils.runPyTests delegate
        gradle({
            rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
            tasks('clean')
            tasks('virtualenv')
            tasks('test')
            switches('--continue')
            switches(Config.json.gradle.defaultSwitches.join(' '))
            switches(Config.json.gradle.unittestSwitches.join(' '))
            switches(Config.json.gradle.robotSwitches.join(' '))
            switches(Config.json.gradle.excludeSwitches.join(' '))
        })
    }
    publishers {
        downstreamParameterized {
            trigger('FO_STANDALONE_INTEGRATION_stable') {
                condition('SUCCESS')
                parameters {
                    predefinedBuildParameters {
                        properties('STABLE_BRANCH=${STABLE_BRANCH}')
                    }
                }
            }
        }
        archiveJunit '**/test-report/**/*.xml'
        PublishersUtils.mainJobsPublishers delegate
    }
}

job('FO_FULL_REGRESSION_qa') {
    JobSeedUtils.defaultConfig delegate
    JobSeedUtils.defaultSCMConfig delegate, 'qa'
    label("fo_full_regression")
    triggers {
            scm('H 21 * * *')
        }
    steps {
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/checkSystemConfiguration.sh"))
        StepsUtils.getGradleWrapper delegate, 'wrapper'
        StepsUtils.runPyTests delegate
        gradle({
            rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
            tasks('clean')
            tasks('virtualenv')
            tasks('test :fo-payments-standalone:itest')
            switches('--continue')
            switches(Config.json.gradle.defaultSwitches.join(' '))
            switches(Config.json.gradle.unittestSwitches.join(' '))
            switches(Config.json.gradle.robotSwitches.join(' '))
            switches(Config.json.gradle.excludeSwitches.join(' '))
        })
    }
    publishers {
        archiveJunit '**/test-report/**/*.xml'
        PublishersUtils.mainJobsPublishers delegate
    }
}

job('FO_CERTIFY_FORMATS_VERSION') {
    parameters {
        stringParam('QA_TAGGED_VERSION')
    }
    JobSeedUtils.defaultEnvironmentVariables delegate
    jdk(Config.json.jenkins.jdk)
    label(Config.json.jenkins.label)
    scm {
        git {
            remote {
                url('git@bitbucket.org:kyridev/kyriba-solution.git')
                credentials(Config.json.jenkins.bitbucketKeyID) // robot
            }
            branch('develop')
            extensions {
                relativeTargetDirectory(Config.json.jenkins.jobWorkspace)
                cloneOptions {
                    shallow(true)
                    timeout(120)
                }
            }
        }
    }
    wrappers {
        WrappersHelper.injPasswords delegate
    }
    steps {
        buildNameUpdater {
            fromFile(false)
            fromMacro(true)
            macroTemplate('QA_CERTIFY_${QA_TAGGED_VERSION}')
            macroFirst(false)
            buildName('${BUILD_NAME}')
        }
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/certifyFOVersion.sh"))
    }
}

job('FO_STANDALONE_INTEGRATION_stable') {
    parameters {
        stringParam('STABLE_BRANCH', 'stable')
    }
    JobSeedUtils.defaultConfig delegate
    JobSeedUtils.defaultSCMConfig delegate, '${STABLE_BRANCH}'
    steps {
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/checkSystemConfiguration.sh"))
        shell(Context.readFile("${Config.json.jenkins.ciBuildScripts}/sh/getStableVersion.sh"))
        environmentVariables {
            propertiesFile('$WORKSPACE/ver.properties')
        }
        buildNameUpdater {
            fromFile(false)
            fromMacro(true)
            macroTemplate('$STABLE_VERSION')
            macroFirst(false)
            buildName('${BUILD_NAME}')
        }
        StepsUtils.getGradleWrapper delegate, 'wrapper'
        gradle({
            rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
            tasks('clean')
            tasks('build')
            tasks('-x test')
            switches(Config.json.gradle.defaultSwitches.join(' '))
            switches(Config.json.gradle.robotSwitches.join(' '))
        })
        gradle({
            rootBuildScriptDir(Config.json.jenkins.jobWorkspace)
            tasks(':fo-payments-standalone:itest')
            switches(Config.json.gradle.defaultSwitches.join(' '))
        })
    }
    publishers {
        downstreamParameterized {
            trigger('FO_PUBLISH_TO_STORAGE_stable') {
                condition('SUCCESS')
                parameters {
                    predefinedBuildParameters {
                        properties('STABLE_BRANCH=${STABLE_BRANCH}')
                    }
                }
            }
        }
        PublishersUtils.mainJobsPublishers delegate
    }
}

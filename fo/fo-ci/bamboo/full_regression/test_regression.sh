foversion=`git describe --abbrev=0 --tags`
echo "Performing Full Regression Testing $foversion..."

echo "<<< lscpu output >>>"
lscpu
echo "<<< lscpu output >>>"
$HOME/opt/gradle2/bin/gradle wrapper
chmod +x gradlew
./gradlew -i --refresh-dependencies clean virtualenv test -DrobotNoHtml -Dvirtualenv=/mnt/bamboo-ebs/opt/python-2.7/bin/virtualenv -Dpython=/mnt/bamboo-ebs/opt/python-2.7/bin/python -DbuildNumber=$foversion -DbuildBranch=${bamboo.repository.branch.name} -Drobot -Dsimultaneous -Dexclude=nix_lineendigs_to_review,TO_REVIEW,mt950_linux_spec,perf_volumes,INCORRECT_BATCH_ORDER,mt950_to_review -Dlegacy
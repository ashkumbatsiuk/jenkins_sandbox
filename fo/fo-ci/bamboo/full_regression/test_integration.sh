foversion=`git describe --abbrev=0 --tags`
echo "Integration testing formats standalone $foversion..."

$HOME/opt/gradle2/bin/gradle wrapper
chmod +x gradlew
./gradlew -i --refresh-dependencies build -x test -Dvirtualenv=/mnt/bamboo-ebs/opt/python-2.7/bin/virtualenv -Dpython=/mnt/bamboo-ebs/opt/python-2.7/bin/python
./gradlew -i --refresh-dependencies :fo-payments-standalone:itest -Dvirtualenv=/mnt/bamboo-ebs/opt/python-2.7/bin/virtualenv -Dpython=/mnt/bamboo-ebs/opt/python-2.7/bin/python
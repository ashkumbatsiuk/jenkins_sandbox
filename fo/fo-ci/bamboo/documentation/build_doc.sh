foversion=`git describe --abbrev=0 --tags`
$HOME/opt/gradle2/bin/gradle wrapper
chmod +x gradlew
./gradlew -i clean virtualenv build sphinx -x test -DbuildNumber=$foversion -Dvirtualenv=/mnt/bamboo-ebs/opt/python-2.7/bin/virtualenv -Dpython=/mnt/bamboo-ebs/opt/python-2.7/bin/python
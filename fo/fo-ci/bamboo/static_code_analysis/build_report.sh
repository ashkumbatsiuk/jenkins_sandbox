foversion=`git describe --abbrev=0 --tags`
if [[ ${bamboo.planRepository.branchName} == qa ]]; then
  $HOME/opt/gradle2/bin/gradle wrapper
  chmod +x gradlew
  ./gradlew -i --refresh-dependencies virtualenv build pylint buildPylintReport -x test --stacktrace --debug -Dvirtualenv=/mnt/bamboo-ebs/opt/python-2.7/bin/virtualenv -Dpython=/mnt/bamboo-ebs/opt/python-2.7/bin/python -DbuildNumber=$foversion -DbuildBranch=${bamboo.repository.branch.name}
fi
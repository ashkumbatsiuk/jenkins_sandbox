KYRIBA_SOLUTION_BRANCH="${bamboo.kyriba.solution.branch}"
FO_VERSION="${bamboo.inject.fo.version}"
CURRENT_PLAN="${bamboo_planName}"
CERTIF_MSG="${CURRENT_PLAN} certifying Formats version ${FO_VERSION}"

if [[ ${bamboo.planRepository.branchName} == qa ]]; then
    echo "### ${CERTIF_MSG}"

    git remote add kyriba-solution "https://${bamboo.git.user.name}:${bamboo.git.user.password}@bitbucket.org/kyridev/kyriba-solution.git"
    git config user.email "${bamboo.git.user.email}"
    git config user.name "${bamboo.git.user.name}"

    git fetch kyriba-solution
    git checkout "${KYRIBA_SOLUTION_BRANCH}"

    sed -i "s/foVersionAccepted=.*/foVersionAccepted=${FO_VERSION}/g" ks-versions.properties

    git add ./ks-versions.properties
    git pull --rebase
    git commit -m "${CERTIF_MSG}"
    git push kyriba-solution "${KYRIBA_SOLUTION_BRANCH}"
fi
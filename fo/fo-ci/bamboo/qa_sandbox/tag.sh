buildNumber=${bamboo.buildNumber}
buildNumber=$((buildNumber+984))

if [ '${bamboo.planRepository.branchName}' = 'qa' ]; then
  git remote | grep -q bitbucket || git remote add bitbucket "https://${bamboo.git.user.name}:${bamboo.git.user.password}@${bamboo.git.repo.url}"
  foversion=`printf %05d ${buildNumber}`
  git config --global user.email "${bamboo.git.user.email}"
  git config --global user.name "${bamboo.git.user.name}"
  git tag -a $foversion -m "$foversion"
  git push bitbucket $foversion
  git remote remove bitbucket
fi

if [ '${bamboo.planRepository.branchName}' = 'stable' ]; then
  stableBuild=`grep -o "version = '.*'" build.gradle | grep -oE "'([0-9]+\.)*[0-9]'" | grep -o "[^']*"`
  if [[ stableBuild ]]; then
  git remote | grep -q bitbucket || git remote add bitbucket "https://${bamboo.git.user.name}:${bamboo.git.user.password}@${bamboo.git.repo.url}"
  git tag $stableBuild
  git push bitbucket $stableBuild
  git remote remove bitbucket
  fi
fi
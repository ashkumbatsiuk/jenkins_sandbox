buildNumber=${bamboo.buildNumber}
buildNumber=$((buildNumber+984))

if [ '${bamboo.planRepository.branchName}' = 'qa' ]
then
  foversion=`printf %05d ${buildNumber}`
  url=http://10.0.33.189:8080/job/Delivery_Formats_KVP_TST2/build
  curl -X POST $url -F json="{\"parameter\": [{\"name\":\"appVersion\", \"value\":\"${foversion}\"}]}" --user robot:987654321 || :

fi
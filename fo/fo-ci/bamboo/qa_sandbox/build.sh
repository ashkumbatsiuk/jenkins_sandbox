buildNumber=${bamboo.buildNumber}
buildNumber=$((buildNumber+984))

if [ '${bamboo.planRepository.branchName}' = 'qa' ] || [ '${bamboo.planRepository.branchName}' = 'stable' ]; then
  $HOME/opt/gradle2/bin/gradle wrapper
  chmod +x gradlew
  ./gradlew -i --refresh-dependencies clean virtualenv build -x test -Dvirtualenv=/mnt/bamboo-ebs/opt/python-2.7/bin/virtualenv -Dpython=/mnt/bamboo-ebs/opt/python-2.7/bin/python -DbuildNumber=${buildNumber} -DbuildBranch=${bamboo.repository.branch.name}
  ./gradlew -i pypiDeploy uploadArchives -Dgsutil=/mnt/bamboo-ebs/gsutil/gsutil -DbuildNumber=${buildNumber} -DbuildBranch=${bamboo.repository.branch.name}
fi
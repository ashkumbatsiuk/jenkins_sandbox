buildNumber=${bamboo.buildNumber}
buildNumber=$((buildNumber+984))

tagToExecute=$(echo ${bamboo.repository.branch.name} | cut -f4 -s -d/)
if [ "$tagToExecute" == "" ];then
    tagToExecute=${bamboo.test.tags.to_execute}
fi

echo "                       >>>> Robot Tags Will Be Executed: ${tagToExecute}"

echo "<<< lscpu output >>>"
lscpu
echo "<<< lscpu output >>>"

$HOME/opt/gradle2/bin/gradle wrapper
chmod +x gradlew
./gradlew -i --refresh-dependencies clean virtualenv test -Dinclude=${tagToExecute} -Dexclude=${bamboo.test.tags.to_exclude} -Dpayment_sendback=${bamboo.test.payment_sendback} -Drobot -DrobotNoHtml -x pythonPackage  -x :fo-app-assemble:build -x :fo-web:build -Dvirtualenv=/mnt/bamboo-ebs/opt/python-2.7/bin/virtualenv -Dpython=/mnt/bamboo-ebs/opt/python-2.7/bin/python -DbuildNumber=${buildNumber} -DbuildBranch=${bamboo.repository.branch.name} -Dlegacy
